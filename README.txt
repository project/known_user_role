Known User Role

By Greg Harvey - http://www.drupaler.co.uk 

Sponsors: Defaqto - http://www.defaqto.com
CMS Pros - http://www.cmspros.co.uk 

This module uses cookies to allow previously logged in users to have an 
extended, role-based, set of privileges without logging in fully. 

Install in the usual way. It makes no changes to the database.

Set the role you wish "known" users to have in admin (User management -> 
Known user role) ... it is advised you create a role specifically for 
the purpose, if required, and use with care! Default is Anonymous (e.g. 
for safety it does nothing except a fancy welcome message if you do not 
set a role). 

In the block admin page (Site building -> Blocks) enable the "Known user 
login" block in the region you usually show your login block. This block 
has three states: 

1. No cookie, not logged in: displays standard user login block
2. Cookie, but not logged in: themed welcome message
3. Logged in: themed logged in message

All of these states are presented by theme functions so you may override
them in the template.php file of your theme in the usual way:

1. theme_known_user_login_block() & theme_known_user_login_block_links()
2. theme_known_user_welcome()
3. theme_known_user_welcome() & theme_known_user_logged_in_block_links()

You should only enable the "Known user login" block - it will do the job 
of any others - if you want to use other login blocks (e.g. those in 
LoginToboggan) use the theme functions to expose these blocks instead of 
the ones presented by default. 


Note, this module will work with any module that implements role-based 
security (hook_perm(), user_access(), etc.) correctly. There are some 
modules which do not - the Blog module is one of them! It has an 
additional check, beyond user roles, to make sure there is a positive 
$user->uid. I don't know why it does this, but it means this module will 
not work with Known User Role. All other modules I have tested work 
fine, but if you find another module that behaves like this please raise 
an issue against THAT PROJECT on Drupal.org (NOT against Known User Role 
- this module is not at fault in this case). 

Note also that some other modules may not behave as expected when an
anonymous user has an additional role. An example of this is the Comment
module, which still displays the login/register link, even though you
can comment. Fortunately it does so in a theme function, so if you want
"known" users to be able to comment (with or without approval) you
should put this in the template.php file of your theme to correct it:

/**
 * Theme a "you can't post comments" notice.
 * We need to check if the Known User Role cookie exists
 * See theme_comment_post_forbidden() for original function
 *
 * @param $node
 *   The comment node.
 * @ingroup themeable
 */
function phptemplate_comment_post_forbidden($node) {
  global $user;
  static $authenticated_post_comments;
  
  // additional check added here for the Known User Role cookie:
  if (!$user->uid && !(isset($_COOKIE['known_user_role_uid']) && $_COOKIE['known_user_role_uid'])) {
    if (!isset($authenticated_post_comments)) {
      // We only output any link if we are certain, that users get permission
      // to post comments by logging in. We also locally cache this information.
      $authenticated_post_comments = array_key_exists(DRUPAL_AUTHENTICATED_RID, user_roles(TRUE, 'post comments') + user_roles(TRUE, 'post comments without approval'));
    }
    
    if ($authenticated_post_comments) {
      // We cannot use drupal_get_destination() because these links
      // sometimes appear on /node and taxonomy listing pages.
      if (variable_get('comment_form_location_'. $node->type, COMMENT_FORM_SEPARATE_PAGE) == COMMENT_FORM_SEPARATE_PAGE) {
        $destination = 'destination='. drupal_urlencode("comment/reply/$node->nid#comment-form");
      }
      else {
        $destination = 'destination='. drupal_urlencode("node/$node->nid#comment-form");
      }

      if (variable_get('user_register', 1)) {
        // Users can register themselves.
        return t('<a href="@login">Login</a> or <a href="@register">register</a> to post comments', array('@login' => url('user/login', array('query' => $destination)), '@register' => url('user/register', array('query' => $destination))));
      }
      else {
        // Only admins can add new users, no public registration.
        return t('<a href="@login">Login</a> to post comments', array('@login' => url('user/login', array('query' => $destination))));
      }
    }
  }
}


Note, The 'known_user_role_details' cookie is created by this module when 
a user logs in, and it will persist for a month, storing the user's id and 
firstname. With Caching turned on in Drupal, there can be problems with the 
names being displayed in the login block, and the problem is fixed by using 
a script to apply the last login name to the home page. For the script in 
this module to work, certain elements need to be applied to the theme 
template.php file. Namely '<strong class="welcome">' and '<strong class="notyou">'.

For example...
/**
 * override the welcome message provided by the known_user_role
 * module here
 */
function phptemplate_known_user_welcome($known_user) {
  global $user;
  
  $output ='';

  $output .= '<div class="logged-in">';
  $output .= '<span class="welcome">';
  $output .= t('Welcome <strong class="welcome"></strong>');
  
  if (!$user->uid || $user->uid == 0) {
    $output .= '</span><div class="known-user-reset">';
    $output .= '<strong class="notyou"></strong> ';
    $output .= l('Click here to login.', 'known-user/reset');
    $output .= '</div>';
  } else {
    $output .= '</span>';
    $output .= '<div class="logout">';
    $output .= l('<span>'.t('Logout').'</span>', 'logout',array('attributes' => array('class' => t('logout-btn'), 'title' => t('Logout')), 'html' => TRUE));
    $output .= '</div>';
  }
  $output .= '</div>';
  return $output;
}
