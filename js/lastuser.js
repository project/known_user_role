/*
 * Reads the user cookie and populates the login block with the 
 * first name of the last logged in user. If a username is found, 
 * add the question 'Not you?' to prompt for a login.
 * 
 * Note that the user first name in the cookie is its second element
 */

Drupal.behaviors.myModuleBehavior = function(context){
  var userdetails = readCookie("known_user_role_details"); 
  if (userdetails != null) {
    // Note that "%7C" corresponds to "|"
    var firstname = userdetails.split("%7C")[1];
    $("strong.welcome").append(firstname);
    $("strong.notyou").append("Not you?");
  }
}

// Given the name of a cookie, return the associated value
function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}